Anagram generator in Ruby.

Assumes list of words is found at `/usr/share/dict/words`

One may replace that path with the location of a file that is a list of words that consists of one word per line.

`anagram.rb` above is a command line script.

Directory `web-anagram` above is a Ruby CGI backend that serves AJAX requests for anagrams, a Javascript front end script that issues the requests, and an HTML file from which to collect user inputs and deposit anagram lists from the backend. A live demo can be seen at [https://astoundingteam.com/anagram](https://astoundingteam.com/anagram).