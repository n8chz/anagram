function wait(whether) {
  $("#anagrams").css("opacity", whether ? 0.5 : 1.0);
}

function button(word, data, type) {
  let b = $("<button>");
  b.attr("type", "button");
  b.attr("data-word", word);
  b.text(type);
  let keyword = type == "+" ? "with" : "without";
  b.addClass(keyword);
  b.attr("title", `include only acronyms ${keyword} ${word}`);
  b.click(function (event) {
    keyword = $(this).attr("class");
    if (data[keyword].indexOf(word) == -1) {
      data[keyword].push(word);
    }
    requestAnagrams(data);
  });
  return b;
}

function requestAnagrams(data = {with: [], without: []}) {
  data.phrase = $("#phrase").val();
  requestData = {
    phrase: data.phrase,
    with: data.with.join(" "),
    without: data.without.join(" ")
  };
  wait(true);
  $.getJSON("./anagram.rb", requestData, function (result) {
    let anagrams = $("#anagrams");
    anagrams.empty();
    let usedWords = [];
    for (let anagram of result) {
      div = $("<div>");
      div.addClass("anagram");
      for (let word of anagram) {
        span = $("<span>");
        span.addClass("word");
        span.text(word);
        if (usedWords.indexOf(word) == -1) {
          span.prepend(button(word, data, "+"));
          span.append(button(word, data, "-"));
          usedWords.push(word)
        }
        div.append(span);
      }
      anagrams.append(div);
      wait(false);
    }
  });
}

$(function () {
  $("#phrase").focus();
  $("#phrase").keydown(function (event) {
    if (event.which == 13) {
      event.preventDefault(); // h/t Phil Carter https://stackoverflow.com/a/895231/948073
      requestAnagrams();
    }
  });
  $("#phrase").keyup(function (event) {
    $("#phrase")
      .val($("#phrase").val().toLowerCase()
      .replace(/\W{2,}/, " ").replace(/[^a-z ]/,""));
  });
  $("#find").click(function () {
    requestAnagrams();
  });
  $("#reset").click(function () {
    $("#phrase").val("");
    $("#phrase").focus();
    $("#anagrams").empty();
  });
});
